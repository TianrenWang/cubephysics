﻿/****************************************
 * Created by Frank Tianren Wang
 * fwang100@uottawa.ca
 * 
 * This is a class to represent data points.
 * *************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataPoint{
    private float time;
    private float value;

    public DataPoint(float time, float value){
        this.time = time;
        this.value = value;
    }

    public float getTime(){
        return time;
    }

    public float getValue(){
        return value;
    }
}
