﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCube : MonoBehaviour {

    //Teleports the cube back to the starting position to avoid having it run endlessly in worldspace
	private void OnTriggerExit(Collider other)
    {
        transform.position = new Vector3(-transform.position.x + 2 * transform.localScale.x, transform.position.y, 0);
    }
}
