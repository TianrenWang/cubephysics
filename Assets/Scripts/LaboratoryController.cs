﻿/****************************************
 * Created by Frank Tianren Wang
 * fwang100@uottawa.ca
 * *************************************/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LaboratoryController : MonoBehaviour {

    private TestCube cube;
    private Material cubeMaterial;
    public static Rigidbody rb;
    private Experiment experiment;
    public Canvas results;
    public Image dot;
    public static bool stoppingCube;

    //Buttons
    public Button push;
    public static Button Push;
    public Button startExperimentButton;
    public static Button StartExperimentButton;
    public Button applyButton;
    public static Button ApplyButton;
    public Button refreshButton;
    public static Button RefreshButton;
    public Button stopButton;

    //Parameters
    public Parameter force;
    public Parameter mass;
    public Parameter kineticFriction;
    public Parameter staticFriction;
    public Parameter time;

    //Status Bars and Values
    public Text speedStatus;
    public Text accelerationStatus;
    public Text remainingTimeStatus;
    public Text distanceStatus;

    //Physics/Experiment Calculations
    private float previousSpeed;
    private float kineticFrictionValue;
    private float staticFrictionValue;
    private ConstantForce constantForce;
    private float remainingTime;
    public static float distanceTravelled;
    public static float acceleration;

    //Notifications
    public NotificationExample experimentEnded;
    public static NotificationExample ExperimentEnded;
    public NotificationExample cannotStartExperiment;
    public NotificationExample speedLimit;
    private bool speedLimitNotificationFlag;

    //UI Graph Objects
    public Image distanceOrigin;
    public Image distanceEnd;
    public Image speedOrigin;
    public Image speedEnd;
    public Image accelOrigin;
    public Image accelEnd;
    public Text x_axis_value;
    public Text y_axis_value;
    public GameObject dataStoragePrefab;
    private GameObject dataStorage;

	// Use this for initialization
    void Start () {
        speedLimitNotificationFlag = true;
        dataStorage = null;
        Push = push;
        StartExperimentButton = startExperimentButton;
        ApplyButton = applyButton;
        RefreshButton = refreshButton;
        ExperimentEnded = experimentEnded;
        rb = GetComponentInChildren<Rigidbody>();
        cube = GetComponentInChildren<TestCube>();
        constantForce = cube.GetComponent<ConstantForce>();
        cubeMaterial = cube.GetComponent<Renderer>().material;
        experiment = null;
        remainingTime = 0;
        previousSpeed = 0;
        force.setDefault(1, 10);
        mass.setDefault(1, 1);
        time.setDefault(1, 5);
        applyParameters();
        GetComponentInChildren<Canvas>().gameObject.SetActive(false);
        stoppingCube = false;
        distanceTravelled = 0;
        acceleration = 0;
	}

    //This is for when the user applies the experimental settings (clicks on "Apply" button)
    public void applyParameters(){
        rb.mass = mass.getValue();
    }
	
	// Update is called once per frame
	void Update () {

        //Update the color of the cube when it is being pushed
        if (!constantForce.enabled){
            cubeMaterial.color = new Color(Mathf.Lerp(cubeMaterial.color.r, 0, Time.deltaTime), 
                                           Mathf.Lerp(cubeMaterial.color.g, 0.736f, Time.deltaTime),
                                           Mathf.Lerp(cubeMaterial.color.b, 1, Time.deltaTime));
        } else {
            cubeMaterial.color = new Color(Mathf.Lerp(cubeMaterial.color.r, 1, Time.deltaTime), 
                                           Mathf.Lerp(cubeMaterial.color.g, 0, Time.deltaTime),
                                           Mathf.Lerp(cubeMaterial.color.b, 0, Time.deltaTime));
        }
	}

	private void FixedUpdate()
	{
        //Update the physics parameters
        constantForce.force = new Vector3(force.getValue(), 0, 0);
        kineticFrictionValue = kineticFriction.getValue();
        staticFrictionValue = staticFriction.getValue();

		//Simulating friction
        if (!Mathf.Approximately(rb.velocity.x, 0) && rb.velocity.x > 0){
            rb.AddForce(new Vector3(-kineticFrictionValue, 0, 0));
        } else if (constantForce.enabled){
            if (constantForce.force.x <= staticFrictionValue || constantForce.force.x <= kineticFrictionValue)
            {
                rb.Sleep();
            }
        }

        if (rb.velocity.x < 0){
            rb.velocity = Vector3.zero;
        }

        if (rb.velocity.x > 100){
            if (speedLimitNotificationFlag)
            {
                speedLimitNotificationFlag = false;
                speedLimit.ShowNotification();
            }
            rb.velocity = new Vector3(100, 0, 0);
        }

        //Update the status
        if ((rb.IsSleeping() || stoppingCube) && experiment != null && experiment.isFinished() || (experiment == null && stoppingCube))
            distanceTravelled = Mathf.Lerp(distanceTravelled, 0, Time.deltaTime * 10);
        else
            distanceTravelled += rb.velocity.magnitude * Time.deltaTime;
        distanceStatus.text = addDecimal((Mathf.Round(distanceTravelled * 10.0f) / 10.0f).ToString());
        speedStatus.text = addDecimal((Mathf.Round(rb.velocity.magnitude * 10.0f) / 10.0f).ToString());
        accelerationStatus.text = addDecimal((Mathf.Round(getAcceleration() * 10.0f) / 10.0f).ToString());
        acceleration = getAcceleration();
        previousSpeed = rb.velocity.magnitude;

        //Stopping the cube
        if (stoppingCube){
            rb.velocity = new Vector3(Mathf.Lerp(rb.velocity.x, 0, Time.deltaTime * 10), 0, 0);
            remainingTime = Mathf.Lerp(remainingTime, 0, Time.deltaTime * 10);
            if (rb.velocity.x < 0.00001 && remainingTime < 0.001 && distanceTravelled < 0.001){
                rb.Sleep();
                stoppingCube = false;
                stopButton.interactable = true;
            }
        }

        //Update the experiment progress
        if (experiment != null && !experiment.isFinished())
        {
            experiment.updateTime();
            experiment.updateData();
            if (0 > remainingTime - Time.deltaTime)
                remainingTime = 0;
            else
                remainingTime -= Time.deltaTime;
        }
        remainingTimeStatus.text = addDecimal((Mathf.Round(remainingTime * 10.0f) / 10.0f).ToString());
	}

    //Sets up the experiment
	public void startExperiment(){
        if (rb.IsSleeping() && !stoppingCube)
        {
            experiment = new Experiment(time.getValue());
            constantForce.enabled = true;
            remainingTime = time.getValue();
            push.interactable = false;
            startExperimentButton.interactable = false;
            refreshButton.interactable = false;
            applyButton.interactable = false;
            distanceTravelled = 0;
        }else{
            cannotStartExperiment.ShowNotification();
        }
    }

    //Rpaidly stops the cube's movement
    public void stopTheCube(){
        if (experiment != null && !experiment.isFinished())
            experiment.finishExperiment();
        stoppingCube = true;
        stopButton.interactable = false;
    }

    public void drawResult(){
        if (experiment != null && experiment.isFinished())
        {
            if (dataStorage != null)
                Destroy(dataStorage);
            dataStorage = Instantiate(dataStoragePrefab, results.GetComponentInChildren<DataStorage>().transform);
            drawGraph(experiment.speed, speedOrigin, speedEnd);
            drawGraph(experiment.acceleration, accelOrigin, accelEnd);
            drawGraph(experiment.distance, distanceOrigin, distanceEnd);
        }
    }

    public void downloadResult(){
        //DirectoryInfo di = new DirectoryInfo(@"/Users/frank/Desktop");
        System.IO.File.WriteAllText(@"/Users/frank/Desktop", "nothing");
    }

    //Calculates the acceleration for each frame and returns it
    private float getAcceleration(){
        return (rb.velocity.magnitude - previousSpeed)/Time.deltaTime;
    }

    //Adds a decimal place to a number in string format if it is an integer
    private string addDecimal(string value){
        if (!value.Contains("."))
            return value + ".0";
        return value;
    }

    //dataPoints - the data points to be plotted
    //graphOrigin - the origin (0, 0) of the graph
    //graphEnd - the far opposite end of the graph
    private void drawGraph(List<DataPoint> dataPoints, Image graphOrigin, Image graphEnd){
        //how far each consecutive data point should be apart from each other
        float spacingX_Axis = (graphEnd.rectTransform.anchoredPosition.x - graphOrigin.rectTransform.anchoredPosition.x) / dataPoints.Count;

        float maximumValue = dataPoints[dataPoints.Count - 1].getValue();
        float minimumValue = dataPoints[0].getValue();

        foreach(DataPoint dataPoint in dataPoints){
            if (maximumValue < dataPoint.getValue())
                maximumValue = dataPoint.getValue();
            if (minimumValue > dataPoint.getValue())
                minimumValue = dataPoint.getValue();
        }

        //the conversion from data point to pixel
        float y_axis_conversionFactor = (graphEnd.rectTransform.anchoredPosition.y - graphOrigin.rectTransform.anchoredPosition.y) /
            (maximumValue - minimumValue);

        //plots each point
        int i = 0;
        foreach (DataPoint dataPoint in dataPoints)
        {
            Image datapoint = (Image)Instantiate(dot, dataStorage.transform);
            datapoint.rectTransform.anchoredPosition = 
                new Vector2(graphOrigin.rectTransform.anchoredPosition.x + i * spacingX_Axis, 
                            graphOrigin.rectTransform.anchoredPosition.y + (dataPoint.getValue() - minimumValue) * y_axis_conversionFactor);
            i++;
        }


        //write the values on each axis
        Text x_min_value = (Text)Instantiate(x_axis_value, dataStorage.transform);
        x_min_value.rectTransform.anchoredPosition = 
            new Vector2(graphOrigin.rectTransform.anchoredPosition.x, graphOrigin.rectTransform.anchoredPosition.y - 15);
        x_min_value.text = Mathf.Round(dataPoints[0].getTime()).ToString();

        Text x_max_value = (Text)Instantiate(x_axis_value, dataStorage.transform);
        x_max_value.rectTransform.anchoredPosition =
                new Vector2(graphEnd.rectTransform.anchoredPosition.x, graphOrigin.rectTransform.anchoredPosition.y - 15);
        x_max_value.text = Mathf.Round(dataPoints[dataPoints.Count - 1].getTime()).ToString();
        
        Text y_max_value = (Text)Instantiate(y_axis_value, dataStorage.transform);
        y_max_value.rectTransform.anchoredPosition =
                new Vector2(graphOrigin.rectTransform.anchoredPosition.x - 35, graphEnd.rectTransform.anchoredPosition.y);
        y_max_value.text = Mathf.Round(maximumValue).ToString();

        Text y_min_value = (Text)Instantiate(y_axis_value, dataStorage.transform);
        y_min_value.rectTransform.anchoredPosition = 
            new Vector2(graphOrigin.rectTransform.anchoredPosition.x - 35, graphOrigin.rectTransform.anchoredPosition.y);
        y_min_value.text = Mathf.Round(minimumValue).ToString();
    }

	//Pause the laboratory
	public void signalSpeedLimitNotificationFlag()
	{
        speedLimitNotificationFlag = true;
	}
}
