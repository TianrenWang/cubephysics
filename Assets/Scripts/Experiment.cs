﻿/****************************************
 * Created by Frank Tianren Wang
 * fwang100@uottawa.ca
 * 
 * This class keeps track of experimental data
 * *************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Experiment{

    private float remainingTime;
    private float elapsedTime;
    private bool finished;

    //The data point list is in order from first collected to last collected
    public List<DataPoint> distance;
    public List<DataPoint> speed;
    public List<DataPoint> acceleration;
	
    public Experiment(float time){
        remainingTime = time;
        elapsedTime = 0;
        distance = new List<DataPoint>();
        speed = new List<DataPoint>();
        acceleration = new List<DataPoint>();
        finished = false;
    }

    //Collects data from the current frame
    public void updateData(){
        if (!finished){
            distance.Add(new DataPoint(elapsedTime, LaboratoryController.distanceTravelled));
            speed.Add(new DataPoint(elapsedTime, LaboratoryController.rb.velocity.magnitude));
            acceleration.Add(new DataPoint(elapsedTime, LaboratoryController.acceleration));
        }
    }

    //Updates the experiment status every frame
    public void updateTime(){
        if (remainingTime > 0)
        {
            remainingTime -= Time.deltaTime;
            elapsedTime += Time.deltaTime;
        }
        else{
            finishExperiment();
        }
    }

    public bool isFinished(){
        return finished;
    }

    public void finishExperiment()
    {
        finished = true;
        LaboratoryController.stoppingCube = true;
        LaboratoryController.Push.interactable = true;
        LaboratoryController.ApplyButton.interactable = true;
        LaboratoryController.RefreshButton.interactable = true;
        LaboratoryController.StartExperimentButton.interactable = true;
        LaboratoryController.rb.transform.GetComponent<ConstantForce>().enabled = false;
        LaboratoryController.ExperimentEnded.ShowNotification();
    }
}
