﻿/****************************************
 * Created by Frank Tianren Wang
 * fwang100@uottawa.ca
 * 
 * 
 * This class controls the behavior of each parameter user can control
 * in the Experimental Setting menu.
 * *************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Parameter : MonoBehaviour {

    private Slider slider;
    private InputField input;
    private Dropdown dropdown;

	// Use this for initialization
	void Start () {
        slider = GetComponentInChildren<Slider>();
        input = GetComponentInChildren<InputField>();
        dropdown = GetComponentInChildren<Dropdown>();
        slider.minValue = 0;
        slider.maxValue = 10;
        input.text = (Mathf.Round(slider.value * 10.0f) / 10.0f).ToString();
	}

    //Synchronizes input field to the slider value
    public void dragged(){
        input.text = (Mathf.Round(slider.value * 10.0f) / 10.0f).ToString();
    }

    //Synchronizes slider value to the input field
    public void inputEntered(){
        float value = float.Parse(input.text);
        if (slider.minValue > value){
            slider.value = slider.minValue;
            input.text = slider.minValue.ToString();
        } else {
            slider.maxValue = value * 2;
            slider.value = value;
        }
    }

    //Sets the default value for parameters (when the software starts)
    public void setDefault(int option, float value){
        dropdown.value = option;
        slider.value = value;
        input.text = (Mathf.Round(slider.value * 10.0f) / 10.0f).ToString();
    }


    //Returns the currently specified value of the parameter
    //Converts the specified values to the standard units
    //The standard units are N, kg, and second
    public float getValue(){
        if (dropdown.options[dropdown.value].text.Equals("mN"))
            return float.Parse(input.text) / 1000.0f;
        else if (dropdown.options[dropdown.value].text.Equals("N"))
            return float.Parse(input.text);
        else if (dropdown.options[dropdown.value].text.Equals("kN"))
            return float.Parse(input.text) * 1000.0f;
        else if (dropdown.options[dropdown.value].text.Equals("g"))
            return float.Parse(input.text) / 1000.0f;
        else if (dropdown.options[dropdown.value].text.Equals("kg"))
            return float.Parse(input.text);
        else if (dropdown.options[dropdown.value].text.Equals("t/Mg"))
            return float.Parse(input.text) * 1000.0f;
        else if (dropdown.options[dropdown.value].text.Equals("μ"))
            return float.Parse(input.text) * Physics.gravity.magnitude * LaboratoryController.rb.mass;
        else if (dropdown.options[dropdown.value].text.Equals("millisecond"))
            return float.Parse(input.text) / 1000.0f;
        else if (dropdown.options[dropdown.value].text.Equals("second"))
            return float.Parse(input.text);
        else if (dropdown.options[dropdown.value].text.Equals("minute"))
            return float.Parse(input.text) * 60;
        else if (dropdown.options[dropdown.value].text.Equals("hour"))
            return float.Parse(input.text) * 3600;
        
        return 1337;
    }
}
